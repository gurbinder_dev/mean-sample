import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup , Validators } from '@angular/forms';
import {HttpModule, Http,Response} from '@angular/http';
import { DashboardService }    from './dashboard.service';

import { Task } from './person';




@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
providers: [DashboardService]
})

export class DashboardComponent implements OnInit {

  SaveTaskForm : FormGroup;
  	responseStatus:Object= [];
  tasks = [];
  counter = 0;
  submitted = false;
  task;
  TaskList: object = [];
  
  constructor( private fb: FormBuilder , private router: Router , public _http: Http , private _dashboarddservice: DashboardService)
  {
	  this.SaveTaskForm = fb.group({
		'taskName' : ['',Validators.required],
		'taskDesc' : ['',Validators.required],
	});

  }


  ngOnInit()
  {
	this.GetTask();
  }
  
  getData()
  {
    this._dashboarddservice.adduser().subscribe(
      data => {
        if(data.success)
        {
        }
      },
      err => console.log(err)
   );
  }
  
  SaveTask( value : any )
  {
	this.responseStatus = [];
    if( !this.SaveTaskForm.valid )
    {
      return false;
    }
	console.log(value);
    this.task = new Task(value.taskName,value.taskDesc);
	console.log(this.task);
	 this.tasks.push(this.task);
    this._dashboarddservice.SaveTask(JSON.stringify(this.task)).subscribe(
      data => {
	  	this.responseStatus = data;
        if( data.success)
        {
			this.GetTask();
        }
      },
      err => console.log(err)
   );
  }
  
  GetTask()
  {
	console.log('get called');
    this._dashboarddservice.GetTask().subscribe(
      data => {
        if( data.success)
        {
			this.TaskList = data.data
			console.log(data);
        }
      },
      err => console.log(err)
   );  
  }


}
