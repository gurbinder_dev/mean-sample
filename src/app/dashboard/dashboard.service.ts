import { Injectable }              from '@angular/core';
import {HttpModule, Http,Response} from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class DashboardService {

    http: Http;
    returnCommentStatus:Object = [];
    constructor(public _http: Http)
    {
        this.http = _http;
    }


    adduser()
    {
      let headers = new Headers();
      headers.append('Content-Type','application/x-www-form-urlencoded;charset=UTF-8');
      return this.http.post('api/create-employee/', { headers }).map(
            (res: Response) => res.json() || {});
    }
	
	SaveTask(data:any)
	{
		console.log(data);
      let headers = new Headers();
		headers.append('Content-Type', 'application/json');
      return this.http.post('http://localhost:3000/createUsers', data ,{ headers }).map(
            (res: Response) => res.json() || {});
	}
	
	GetTask()
	{
      return this.http.get('http://localhost:3000/seeResults').map(
            (res: Response) => res.json() || {});
	}

}
