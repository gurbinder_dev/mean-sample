import { Component,OnInit, Injectable } from '@angular/core';
import { Person } from './person';
import { FormsModule } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
// We need it because of the `map` method we use below
import { Observable } from 'rxjs/Observable';


@Component({
  moduleId: module.id,
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{ 
  constructor(public http: Http) { }

  persons = [];
  
  counter = 0;
  submitted = false;
  person;
  onSubmit(value) {
    this.person = new Person('test', 'test@gmail.com', '10');
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    if (value) {
      this.persons.push(this.person);
    }
    this.counter++;
    this.submitted = true;
    this.http.post('http://localhost:3000/createUsers',
      JSON.stringify(this.person), { headers: headers })
      .subscribe(err => console.log(err));
  }
  
  ngOnInit()
  {
	this.onSubmit('test');
  }
  
  getUsers() {
    this.http.get('http://localhost:3000/seeResults')
     .map((res: Response) => res.json())
     .subscribe((res: any) => {
       this.persons = res;
	   console.log(this.persons);
     });
  }
}

