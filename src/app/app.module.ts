import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { HttpModule } from '@angular/http';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';


const appRoutes: Routes = [

  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'posts',
    component: PostsComponent,
	data: { title: 'Dibcase | Register' }
  },
  {
    path: 'register',
    component: RegisterComponent
  },
{
    path: 'dashboard',
    component: DashboardComponent,
	data: { title: 'Dibcase | Register' }
  }



];


@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    RegisterComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
	RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
